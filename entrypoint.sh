#!/bin/sh -x

echo "SoftClouds123" | /usr/bin/vncpasswd -f >/home/okm/passwd

/usr/bin/vncserver \
  :0 \
  -geometry 1920x1080 \
  -fg \
  -PasswordFile "/home/okm/passwd" \
  -xstartup /home/okm/xstartup.sh \
  -interface 0.0.0.0 \
  -CompareFB=0
