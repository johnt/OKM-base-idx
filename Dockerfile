FROM oraclelinux:7.1
LABEL maintainer="johnt@softclouds.com"
#1521 Oracle DB
#5500 Oracle DB
#8080 System Manager
#8222 Indexer (System Manager)
#8223 Runtime
#8226 IM/IC/IMWS
#9000 Indexer sync port
#9001 Runtime sync port
EXPOSE 1521 5500 8080 8222 8223 8226 9000 9001
RUN yum install tar -y        && \
    yum install net-tools -y  && \
    yum install iputils -y  && \
    yum install wget -y       && \
    yum install curl -y       && \
    rm -rf /var/cache/yum     && \
    mkdir /var/cache/yum      
#Create the /opt directory. Continue if it already exists.
RUN mkdir /opt; exit 0
WORKDIR /opt
RUN wget https://gitlab.com/johntang/OKM-base/raw/master/apache-tomcat-7.0.57.tar && \
    tar xfv apache-tomcat-7.0.57.tar && \
    rm apache-tomcat-7.0.57.tar && \
    rm -rf /opt/apache-tomcat-7.0.57/webapps/docs && \
    rm -rf /opt/apache-tomcat-7.0.57/webapps/examples
RUN wget https://gitlab.com/johntang/OKM-base/raw/master/jdk-8u31-linux-x64.rpm && \
    rpm -Uvh jdk-8u31-linux-x64.rpm       && \
    rm /opt/jdk-8u31-linux-x64.rpm        && \
    rm -rf /usr/java/jdk1.8.0_31/man      && \
    rm -rf /usr/java/jdk1.8.0_31/src.zip  && \
    rm -rf /usr/java/jdk1.8.0_31/javafx-src.zip
WORKDIR /opt/apache-tomcat-7.0.57/conf/
RUN rm tomcat-users.xml && \
    wget https://gitlab.com/johntang/OKM-base/raw/master/tomcat-users.xml 
WORKDIR /opt 
RUN useradd okm && \
    chown -R okm:okm /opt
USER okm
ENV CATALINA_HOME=/opt/apache-tomcat-7.0.57
ENV CATALINA_BASE=$CATALINA_HOME
ENV JAVA_HOME=/usr/java/jdk1.8.0_31
WORKDIR /opt/apache-tomcat-7.0.57/