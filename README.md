# Docker with VNC Server

Docker container for Oracle Linux 7.1 including vncserver.


# How to build

`docker build -t softclouds:vnc .`


# How to run

`docker run -d --name vnc -p 5900:5900 -v /tmp:/transfer softclouds:vnc`
or 
`docker-compose up -d`


and then connect to:

`vnc://<host>:5900` via VNC client.  See https://chrome.google.com/webstore/detail/vnc%C2%AE-viewer-for-google-ch/iabmpiboiopbgfabjmgeedhcmjenhbla?hl=en

The VNC password is `SoftClouds123`.
